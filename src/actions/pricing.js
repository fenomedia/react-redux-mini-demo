
export const updatePrice = (value, type) => dispatch => {
    dispatch({
        type: type,
        payload: value
    });
}