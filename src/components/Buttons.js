import React from 'react'
import { updatePrice } from '../actions/pricing'
import { INCREMENT_PRICE, DECREMENT_PRICE } from '../actions/actionTypes'
import { connect } from 'react-redux';

const Buttons = function({updatePrice}) {
    return (
        <div>
            <button onClick={() => updatePrice(10, INCREMENT_PRICE)}>Erhöhen</button>
            <button onClick={() => updatePrice(10, DECREMENT_PRICE)}>Verringern</button>
        </div>
    )
}

export default connect(null, {updatePrice})(Buttons);

