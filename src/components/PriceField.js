import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';

function PriceField({price, mwst}) {
    return (
        <div>
            Aktueller Preis {price} Euro inkl. {mwst} Euro Mwst.
        </div>
    )
}

PriceField.propTypes = {
    price: PropTypes.number.isRequired
}

const mapStateToProps = state => ({
    price: state.pricing.price,
    mwst: state.pricing.mwst,
});

export default connect(mapStateToProps)(PriceField);

