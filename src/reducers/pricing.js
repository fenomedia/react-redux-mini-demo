import { INCREMENT_PRICE, DECREMENT_PRICE } from '../actions/actionTypes';

const initialState = {
    price: 0,
    mwst: 0
};


export default function(state = initialState, action) {
    const {type, payload} = action;
    let finalPrice = 0;
    switch(type){
        case INCREMENT_PRICE:
            finalPrice = state.price + payload;
        break;
        case DECREMENT_PRICE:
            finalPrice = state.price - payload;
        break;
        default:
            return state;
    }

    const mwst = Number(finalPrice * .19 / 1.19).toFixed(2);
    return {
        ...state,
        price: finalPrice,
        mwst: mwst
    }
}