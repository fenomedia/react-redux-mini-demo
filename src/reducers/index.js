import { combineReducers } from 'redux';
import pricing from './pricing';

export default combineReducers({
    pricing
});