import React from 'react';
import './App.css';
import PriceField from './components/PriceField'
import { Provider } from 'react-redux';
import store from './store';
import Buttons from './components/Buttons'


function App() {
  return (
    <Provider store={ store }>
      <div className="App">
        <PriceField/>
        <Buttons/>
      </div>
    </Provider>
  );
}

export default App;
